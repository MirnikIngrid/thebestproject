# Sodelujoči

Prispevki sodelujočih k projektu.

## Plečko Ana
* ER - diagram,
* DPU,
* Google Maps,
* Sestavljanje tacota,
* Podatkovna baza - vnos podatkov,
* Admin strani,
* Dinamičnost strani,
* XML.

## Bobnar Matic
* Vzpostavitev baze podatkov,
* Postavitev Spring projekta, 
* Registracija,
* Prijava,
* Admin strani, 
	* Vnos novih artiklov/živil,
	* Funkcionalnost pregleda naročil.
* XML,
* Seje,
* Dinamičnost strani,

## Hauptman Nejc
* Template
* Ureditev templata
* Funkcionalnost obveščanja (email)
	* Rezervacija miz,
	* Kontakt, 
	* Naročila.
* Admin strani,
* XML,
* Seje.

## Mirnik Ingrid
* E-trgovina,
* Dinamičnost strani,
* Menu,
* Košarica,
* Plačilo,
* Seje,
* .md datoteke,
* XML.