﻿SET DATEFORMAT DMY;
	
INSERT INTO Tip_Zivila(id_Tip_Zivila,naziv) 
VALUES(1,'Taco'),
      (2,'Pijača'),
	  (3,'Sladica'),
	  (4,'Sestavina Zelenjava'),
	  (5,'Sestavina Meso'),
	  (6,'Sestavina Omaka'),
	  (7,'Sestavljen Taco');
GO

INSERT INTO Vrsta_Placila (id_Vrsta_Placila, naziv)
VALUES(1,'PayPal'),
      (2,'Ob prevzemu');
GO

INSERT INTO Posta(kraj,postna_Stevilka)
VALUES('Dramlje',3222),
      ('Celje',3000),
	  ('Radizel',2312),
	  ('Kamnica',2351);
GO

INSERT INTO Naslov(naslov,fk_Posta)
VALUES('Metropola 53',1),
      ('Štajerska prestolnica 1',2),
	  ('Nek pod Mariborom 5',3),
	  ('Hollywood 16',4);
GO

delete from Narocilo
delete from Zivilo

INSERT INTO Zivilo(naziv,opis,cena,slika_Zivila,fk_Tip_Zivila) VALUES
('Paradižnik','Paradižnik je dobesedno nabit z zdravimi snovmi in vitamini.',1.0,'images/taco_slike/par.png',4),
('Črni fizol','Fižol je okusna, zdrava in poceni hrana, polna vitaminov in koristnih snovi.',1.0,'images/taco_slike/fizol.png',4),
('Solata','Solate so polne vitaminov, saj so pripravljene iz sveže zelenjave in sadja.',1.0,'images/taco_slike/solata.png',4),
('Japaleno','Ful peče. ',1.0,'images/taco_slike/jalapeno.png',4),
('Steak','Meso za steake mora biti dobro uležano. Bolj ko je prepredeno z maščobo (marmorirano), bolj okusno je.',1.0,'images/taco_slike/steak.png',5),
('Pečen piščanec','Hrustljava koža in sočno meso: to je vrhunsko pripravljen pečen piščanec. Skrivnost je v času priprave, izboru pravih začimb in kakovostnih kuharskih pripomočkov.',1.0,'images/taco_slike/pisce.png',5),
('Teriyaki piščanec','Tole ni navadni piščanec, to je slastno doživetje, ki bo goste popeljalo v kulinarična nebesa. Brez pretiravanja. To jed pripravim vsakič, ko nekdo prvič pride na obisk in raven zadovoljstva gostov je 100%.',1.0,'images/taco_slike/piscanec.png',5),
('Govedina','Govedina vsebuje veliko hranilnih snovi, rudnin, železa in cinka.Goveje mesto je danes bolj pusto kot nekoč, zaradi sodobnega načina reje.',1.0,'images/taco_slike/govedina.png',5),

('Kamniški Pekoči Taco','500g piščančjega mesa, veganski sir, sveži chilliji, popečena sveža zelenjava, solata in hišna pekoča omaka.',10,'images/taco_slike/tacobellStiri.jpg',1),
('Šentjurski BG Specialci','250g domače svinjine, zaseka, popečena sveža zelenjava, solata, paradižnik, gorčica ter brezglutenska tortilija.',10,'images/taco_slike/tacobellEna.jpg',1),
('Mladi Radizelčan','250g piščančjega mesa, mešanica mozzarella in nacho sira, testenine, popečena sveža zelenjava, sojina omaka.',10,'images/taco_slike/tacobellTri.jpg',1),
('Fit Celjanar','250g piščančjega mesa popečenega na kokosovi masti, 1.3% mozzarella sir, sveži chilliji, popečena sveža zelenjava, riž.',10,'images/taco_slike/tacobellDva.jpg',1),
('Fsipani Taco','Piščančje meso marinirano v olju, solata, chilli, koruza, paradižnik in brezglutenska tortilija.',4.9,'images/taco_slike/tacobellSedem.jpg',1),
('GremoVMangota Taco','500g piščančjega mesa, veganski sir, sveži chilliji, popečena sveža zelenjava, solata in hišna pekoča omaka.',5.6,'images/taco_slike/tacobellSest.jpg',1),
('Mešano na žaru Taco','300g piščančjega mesa, veganski sir, sveži chilliji, popečena sveža zelenjava, solata in hišna pekoča omaka.',5.3,'images/taco_slike/tacobellPet.jpg',1),
('Čista Desetka XXL Taco','XXL 2kg taco. 1kg drameljške govedine, zelenjava iz žara, chilli omaka, smetana, mozzarella sir in pekoča BBQ omaka.',6.9,'images/taco_slike/stiri.jpg',1),

('Pune Dora Torta','Če bi se rad posladkal in ne veš kaj točno bi naročil, s to torto ne moreš zgrešiti.',4.9,'images/dessert-1.jpg',3),
('Darile Torta','Primerna za vse generacije, še posebaj pa pride do svojega čara ob praznovanju.',4.9,'images/dessert-2.jpg',3),
('The Best BG Torta','Kaj te zdaj če ne smeš jest glutena. Najboljše smo prišparali za konec. Poiskusi.',4.9,'images/dessert-3.jpg',3),
('Kafe Rabim Torta','V primeru, da je za tabo naporen dan in veliko programiranja, naroči to torto.',4.9,'images/dessert-4.jpg',3),
('Ena Štajerska','Ko si Bobiju pade cuker ne more brez Ene Štajerkse.',6.9,'images/dessert-5.jpg',3),
('Sacher torta','Gremo v pohorsko kavarno? Pune doro je tam.',5.9,'images/dessert-6.jpg',3),
('Coca Cola','Vse stvari gredo boljše s Coca Colo.',2.5,'images/taco_slike/cocacola.jpg',2),
('Vodjin Napitek','Energijski napitek po receptu naše vodje.',2.9,'images/taco_slike/limonada.jpg',2),
('Jagodni Sok','Jagodni sok narejen iz kamnniških jagod.',2.2,'images/taco_slike/jagoda.jpg',2),
('Vitaminski napitek','Napitek iz sveže stisnjenih radizeljskih pomaranč.',3.2,'images/taco_slike/oranza.jpg',2),
('Pivo','Če je žeja ali pa če boš ravnokar naredil praktikum si naroči pivce.',2.9,'images/taco_slike/pivo.jpg',2),
('Špricer','Žlahtna kapljica za žejne iz Bobnarjovga keudra.',2.9,'images/taco_slike/špricer.jpg',2),
('Tin Gonic','Za tiste najbolj zveste goste.',10,'images/taco_slike/zalucijo.png',2),
('Kremasta','Šefova specialiteta.',0.9,'images/taco_slike/krema.png',6),
('Načo','Šefova specialiteta.',0.9,'images/taco_slike/naco.png',6),
('Ranč','Šefova specialiteta.',0.9,'images/taco_slike/ranc.png',6)
GO