# Taco

Taco.si je mehiška restavracija, ki poskuša ujeti duha Mehike v hrani in ambientu. Ravno zato tudi takšno ime, ki
izvira iz španske besede »taco«, katere prevod pomeni tipično mehiško jed iz pšenične ali koruzne tortilje, polnjene z različnimi nadevi. 
Na spletni strani si lahko registriran uporabnik izdela lasten taco, ter si ga tudi naroči. Naši zaposleni bodo naročilo skrbno
pregledali in poskrbeli, da vam zahtevo uredijo v najkrajšem možnem času ter vas o tem tudi obvestijo preko e-mail sporočila. Preden končno naročilo oddate,
si ga lahko prenesete in kasneje tudi uvozite ter ponovno naročite. 
Prav tako preko spleta sprejemamo tudi rezervacije miz. Za več informacij ste vabljeni k ogledu naše spletne strani.

Ekipa Taco.si

# Namen
Omogočiti lažje in enostavnejše naročanje hrane, ter tako izboljšati zadovoljstvo naših strank.

# Pregled

Funkcionalnosti, ki vam jih, glede na vašo vlogo, ponuja spletno mesto Taco.si.

* **Osnovne funkcionalnosti**
	* Registracija
	* Pregled ponudbe
	* Rezervacija mize
	
* **Dodatne funkcionalnosti za člane**
	* Prijava
	* Sestavljanje poljubnega tacota
	* Spletna trgovina
	* Izvoz in uvoz osebnega naročila
	* Pregled, urejanje in brisanje košarice
	* Naročanje
	
* **Dodatne funkcionalnosti za administratorje**
	* Pregled, urejanje in brisanje naročil
	* Potrditev ali zavrnitev naročil
	* Vnos novih artiklov
	* Urejanje in brisanje artiklov
	
# Uporabljene tehnologije

* Spring MVC Framework
* Java
* JavaScript
* JQuery
* Microsoft SQL Server Managment studio 18
* XML
* [Colorlib tema](https://colorlib.com/)


