CREATE DATABASE Taco
GO
USE Taco
GO
SET DATEFORMAT dmy;  
GO  

CREATE TABLE Posta(
id_Posta INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
kraj NVARCHAR(45) NOT NULL,
postna_Stevilka INT NOT NULL
)

CREATE TABLE Naslov(
id_Naslov INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
naslov NVARCHAR(45) NOT NULL,
fk_Posta INT NOT NULL
)

CREATE TABLE Uporabnik(
id_Uporabnik INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
ime NVARCHAR(45) NOT NULL,
priimek NVARCHAR(45) NOT NULL,
datum_Rojstva DATE NOT NULL,
telefonska_Stevilka NVARCHAR(45) NOT NULL,
elektronska_Posta NVARCHAR(45) NOT NULL,
uporabnisko_Ime NVARCHAR(45) NOT NULL,
geslo NVARCHAR(300) NOT NULL,
vloga INT NOT NULL,
fk_Naslov INT NOT NULL
)

CREATE TABLE Vrsta_Placila(
id_Vrsta_Placila INT PRIMARY KEY NOT NULL,
naziv NVARCHAR(45) NOT NULL
)

CREATE TABLE Narocilo(
id_Narocilo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
skupna_Cena DECIMAL NOT NULL,
datum DATE NOT NULL,
sprejeto INT NOT NULL,
fk_Uporabnik INT NOT NULL,
fk_Vrsta_Placila INT NOT NULL
)

CREATE TABLE Tip_Zivila (
id_Tip_Zivila INT PRIMARY KEY NOT NULL,
naziv NVARCHAR(45) NOT NULL
)

CREATE TABLE Zivilo(
id_Zivilo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
naziv NVARCHAR(45) NOT NULL,
opis NVARCHAR(500) NOT NULL,
cena DECIMAL NOT NULL,
slika_Zivila NVARCHAR(500) NOT NULL,
fk_Tip_Zivila INT NOT NULL
)

CREATE TABLE Narocilo_Zivilo(
id_Narocilo_Zivilo INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
kolicina INT NOT NULL,
fk_Zivilo INT,
fk_Narocilo INT NOT NULL,
)

Alter table Naslov ADD CONSTRAINT Naslov_Posta foreign key(fk_Posta) references Posta (id_Posta)  on delete no action ;
Alter table Uporabnik ADD CONSTRAINT Uporabnik_Naslov foreign key(fk_Naslov) references Naslov (id_Naslov)  on delete no action ;
Alter table Narocilo ADD CONSTRAINT Narocilo_VrstaPlacila foreign key(fk_Vrsta_Placila) references Vrsta_Placila (id_Vrsta_Placila)  on delete no action ;
Alter table Narocilo ADD CONSTRAINT Narocilo_Uporabnik foreign key(fk_Uporabnik) references Uporabnik (id_Uporabnik)  on delete no action ;
Alter table Narocilo_Zivilo ADD CONSTRAINT Narocilo_Zivilo_Zivilo foreign key(fk_Zivilo) references Zivilo (id_Zivilo)  on delete no action ;
Alter table Narocilo_Zivilo ADD CONSTRAINT Narocilo_Zivilo_Narocilo foreign key(fk_Narocilo) references Narocilo (id_Narocilo)  on delete no action ;
Alter table Zivilo ADD CONSTRAINT Zivilo_TipZivila foreign key(fk_Tip_Zivila) references Tip_Zivila (id_Tip_Zivila)  on delete no action ;
