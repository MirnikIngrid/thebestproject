$(document).ready(function () {
    $('#poslji').click(function (event) {
        event.preventDefault()

        var ime = $('#Ime').val();
        var priimek = $('#Priimek').val();
        var datum = $('#Datum').val();
        var cas = $('#Cas').val();
        var telefonska = $('#Telefonska').val();
        var status = $(".status");
        status.empty();

        if (ime.length<3){
           status.append('<div> Ime ni pravilno. </div>')
        }
        if (priimek.length<3){
            status.append('<div>Priimek ni pravilen.</div>')
        }
        if (datum==""){
            status.append('<div> Nepravilno vnesen datum. </div>')
        }
        if (cas==""){
            status.append('<div> Nepravilno vnesen čas. </div>')
        }

        if (telefonska.length<8 ){
            status.append('<div> Telefonska številka ni prava.</div>')
        }
        if ( telefonska.length>11 ){
            status.append('<div> Telefonska številka ne ustreza.</div>')
        }

    })
})