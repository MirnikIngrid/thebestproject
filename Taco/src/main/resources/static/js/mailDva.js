$(document).ready(function () {
    $('#PotrdiDvagi').click(function (event) {
        event.preventDefault()

        var imePriimek = $('#imeP').val();
        var email = $('#email').val();
        var subject = $('#subject').val();
        var sporocilo = $('#sporocilo').val();
        var status = $(".status");
        status.empty();

        //ime in priimek moreta biti skupaj dolga več kot 4 črke
        if (imePriimek.length<4){
            status.append('<div> Ime ni pravilno. </div>')
        }
        //email mora biti dolg več kot 5 znakov, vsebovati more . in @
        if (!email.length>5 || !email.includes('@') || !email.includes('.')){
            status.append('<div> Neustrezen vnos email naslova. </div>')
        }

        //V primeru da je zadeva null se sproži spodnje sporočilo
        if (subject==""){
            status.append('<div> Manjka zadeva. </div>')
        }

    })
})