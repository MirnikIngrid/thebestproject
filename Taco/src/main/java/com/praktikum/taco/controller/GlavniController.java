package com.praktikum.taco.controller;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.praktikum.taco.model.*;
import org.junit.jupiter.api.Test;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import java.io.*;
import java.lang.*;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Controller
@SessionAttributes("artikli")
public class GlavniController {

    static ArrayList<PrikazNarocil> narocila = new ArrayList<PrikazNarocil>();
    static ArrayList<Artikel> artikli = new ArrayList<>();

    @Autowired
    private JavaMailSender sender;

    Connection con = null;
    String connectionString = "jdbc:sqlserver://localhost\\SQLEXPRESS;databaseName=Taco;integratedSecurity=true"; //string z info. za povezavo na lokalno PB.
    /**
     * Metoda za vzpostavljanje povezave z podatkovno bazo.
     *
     * @return povezavo (Connection)
     */
    public Connection getConnection() {
        try {
            con = DriverManager.getConnection(connectionString, "", "");

            return con;
        } catch (Exception sqlcon) {
            sqlcon.printStackTrace();
            return null;
        }
    }

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    @RequestMapping(value = {"/odjava"}, method = RequestMethod.GET)
    public String odjava(Model model, HttpSession Session) {

        ResultSet rs = null;
        Connection con = getConnection();
        Narocilo narocilo = (Narocilo) Session.getAttribute("narocilo");
        int index = narocilo.getIdNarocila();
        int sprejeto = 0;
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT sprejeto FROM Narocilo WHERE id_Narocilo = " + index);
            while (rs.next()) {
                sprejeto = rs.getInt("sprejeto");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (sprejeto == 0) {
            try {
                PreparedStatement insert = con.prepareStatement("DELETE FROM Narocilo WHERE id_Narocilo = " + index);
                insert.executeUpdate();
            } catch (Exception odjava) {
                odjava.printStackTrace();
            }
        }
        Session.invalidate();
        return "index";
    }

    @RequestMapping(value = {"/about"}, method = RequestMethod.GET)
    public String about(Model model) {
        return "about";
    }

    @RequestMapping(value = {"/dodajSestavino"}, method = RequestMethod.GET)
    public String dodajSestavino(Model model, HttpSession session, String index) {
        SestaviTaco x = (SestaviTaco) session.getAttribute("sestaviTaco");
        x.dodajSestavino(index);
        model.addAttribute("sestavine", x.getNazivi());
        ArrayList<Artikel> meso = new ArrayList<>();
        ArrayList<Artikel> zelenjava = new ArrayList<>();
        ArrayList<Artikel> omaka = new ArrayList<>();

        zelenjava = posodobiZelenjavo();
        meso = posodobiMeso();
        omaka = posodobiOmako();
        model.addAttribute("omaka", omaka);
        model.addAttribute("zelenjava", zelenjava);
        model.addAttribute("meso", meso);
        return "blog";
    }

    @RequestMapping(value = {"/odstraniSestavino"}, method = RequestMethod.GET)
    public String odstraniSestavino(Model model, HttpSession session, String index) {
        SestaviTaco x = (SestaviTaco) session.getAttribute("sestaviTaco");
        x.odstraniSestavino(index);
        model.addAttribute("sestavine", x.getNazivi());
        ArrayList<Artikel> meso = new ArrayList<>();
        ArrayList<Artikel> zelenjava = new ArrayList<>();
        ArrayList<Artikel> omaka = new ArrayList<>();

        zelenjava = posodobiZelenjavo();
        meso = posodobiMeso();
        omaka = posodobiOmako();
        model.addAttribute("omaka", omaka);
        model.addAttribute("zelenjava", zelenjava);
        model.addAttribute("meso", meso);
        return "blog";
    }

    @RequestMapping(value = {"/blog"}, method = RequestMethod.GET)
    public String blog(Model model, HttpSession session) {

        ArrayList<Artikel> meso = new ArrayList<>();
        ArrayList<Artikel> zelenjava = new ArrayList<>();
        ArrayList<Artikel> omaka = new ArrayList<>();

        zelenjava = posodobiZelenjavo();
        meso = posodobiMeso();
        omaka = posodobiOmako();
        model.addAttribute("omaka", omaka);
        model.addAttribute("zelenjava", zelenjava);
        model.addAttribute("meso", meso);

        return "blog";
    }


    public ArrayList<Artikel> posodobiZelenjavo() {

        ArrayList<Artikel> dodatkiZelenjava = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND (Zivilo.fk_Tip_Zivila = 4)");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                dodatkiZelenjava.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dodatkiZelenjava;
    }

    public ArrayList<Artikel> posodobiMeso() {

        ArrayList<Artikel> dodatkiMeso = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND (Zivilo.fk_Tip_Zivila = 5)");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                dodatkiMeso.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dodatkiMeso;
    }

    public ArrayList<Artikel> posodobiOmako() {

        ArrayList<Artikel> dodatkiOmaka = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND (Zivilo.fk_Tip_Zivila = 6)");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                dodatkiOmaka.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dodatkiOmaka;
    }


    @RequestMapping(value = {"/izbrisiNarocilo"}, method = RequestMethod.GET)
    public String brisi(Model model, @RequestParam("index") int index) {
        narocila.remove(index);
        model.addAttribute("narocila", narocila);
        return "narocila";
    }

    //###############################################################################################################
    @RequestMapping(value = {"/cart"}, method = RequestMethod.GET)
    public String cart(Model model, HttpSession session) {

        Narocilo narocilo = (Narocilo) session.getAttribute("narocilo");
        ArrayList<Artikel> izbraniArtikli = narocilo.getArtikli();

        double skupniZnesek = 0;
        for (Artikel a : izbraniArtikli) {
            skupniZnesek = skupniZnesek + (a.getKolicina() * a.getCena());
        }
        model.addAttribute("skupniZnesek", skupniZnesek);
        session.setAttribute("skupniZnesek", skupniZnesek);
        model.addAttribute("izbraniArtikli", izbraniArtikli);

        return "cart";
    }

    @RequestMapping(value = {"/naloziXML"}, method = RequestMethod.POST)
    public String naloziXML(Model model, HttpSession session,
                            @RequestParam MultipartFile file) {

        String path = session.getServletContext().getRealPath("/");
        String fileName = file.getOriginalFilename();
        String filePath = path + "\\WEB-INF\\downloads\\" + fileName;
        System.out.println(path + " " + fileName);

        Narocilo narocilo=new Narocilo();
        try {
            narocilo = whenJavaGotFromXmlFile_thenCorrect(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        session.setAttribute("narocilo",narocilo);
        ArrayList<Artikel> izbraniArtikli = narocilo.getArtikli();

        double skupniZnesek = 0;
        for (Artikel a : izbraniArtikli) {
            skupniZnesek = skupniZnesek + (a.getKolicina() * a.getCena());
        }
        model.addAttribute("skupniZnesek", skupniZnesek);
        session.setAttribute("skupniZnesek", skupniZnesek);
        model.addAttribute("izbraniArtikli", izbraniArtikli);
        session.setAttribute("stArtiklov", narocilo.getArtikli().size());


        return "cart";
    }
    //###############################################################################################################
    @RequestMapping(value = {"/prenosXML"}, method = RequestMethod.GET)
    public String prenosXML(HttpServletRequest req, HttpServletResponse response, Model model, HttpSession session) throws IOException {
        //Authorized user will download the file
        Narocilo narocilo = (Narocilo) session.getAttribute("narocilo");

        String path = session.getServletContext().getRealPath("/");
        String filename = "taco.xml";
        String filePath = path + "\\WEB-INF\\downloads\\" + filename;
        System.out.println(path + " " + filename);

        try {

            XmlMapper xmlMapper = new XmlMapper();
            String xml = xmlMapper.writeValueAsString(narocilo);
            xmlMapper.writeValue(new File("E:\\FERI\\1 LETNIK\\2 SEMESTER\\PRAKTIKUM\\thebestproject\\Taco\\src\\main\\webapp\\WEB-INF\\downloads\\taco.xml"), narocilo);
            File file = new File("taco.xml");
            assertNotNull(file);
            assertNotNull(xml);

            OutputStream outputStream = new FileOutputStream(file);
            response.setContentType("application/force-download");
            response.setHeader("Content-Disposition", "attachment; filename=");
            response.flushBuffer();
            outputStream.close();
            whenJavaSerializedToXmlFile_thenCorrect(session, filePath);

            File f = new File(filePath);
            if (f.exists()) {
                response.setContentType("text/xml");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + filename + "\"");


                OutputStream os = response.getOutputStream();
                FileInputStream stream = new FileInputStream(f);
                BufferedInputStream bis = new BufferedInputStream(stream);
                InputStream is = new BufferedInputStream(bis);
                int count;
                byte buf[] = new byte[4096];
                while ((count = is.read(buf)) > -1)
                    os.write(buf, 0, count);
                is.close();
                os.close();
            }
        } catch (Exception exception) {
            System.out.println(":) All is fine :)");
        }


        return "cart";
    }

    @RequestMapping(value = {"/checkout"}, method = RequestMethod.GET)
    public String checkout(HttpSession session,
                           Model model) {

        model.addAttribute("skupniZnesek", session.getAttribute("skupniZnesek"));
        int idUporabnika = (int) session.getAttribute("idUporabnik");

        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Uporabnik.ime as ime, Uporabnik.priimek as priimek, Uporabnik.elektronska_Posta as email, Uporabnik.telefonska_Stevilka as tel, Naslov.naslov as naslov, Posta.kraj as kraj, Posta.postna_Stevilka as posta FROM Uporabnik, Naslov, Posta WHERE Uporabnik.fk_Naslov = Naslov.id_Naslov AND Posta.id_Posta = Naslov.fk_Posta AND Uporabnik.id_Uporabnik =" + idUporabnika);
            while (rs.next()) {
                model.addAttribute("ime", rs.getString("ime"));
                model.addAttribute("priimek", rs.getString("priimek"));
                model.addAttribute("naslov", rs.getString("naslov"));
                model.addAttribute("kraj", rs.getString("kraj"));
                model.addAttribute("posta", rs.getString("posta"));
                model.addAttribute("tel", rs.getString("tel"));
                model.addAttribute("email", rs.getString("email"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return "checkout";
    }

    @RequestMapping(value = {"/contact"}, method = RequestMethod.GET)
    public String contact(Model model) {
        return "contact";
    }

    @RequestMapping(value = {"/menu"}, method = RequestMethod.GET)
    public String menu(Model model) {

        // Potrebni ArrayListi za prikaz različnih živil v ponudbi
        ArrayList<Artikel> glavneJedi = new ArrayList<>();
        ArrayList<Artikel> sladice = new ArrayList<>();
        ArrayList<Artikel> pijaca = new ArrayList<>();
        ArrayList<Artikel> dodatki = new ArrayList<>();

        //posodobitev arraylistov ob prikazi strani
        glavneJedi = posodobiGlavneJedi();
        pijaca = posodobiPijaco();
        sladice = posodobiSladice();
        dodatki = posodobiDodatke();

        //dodani listi na model
        model.addAttribute("glavneJedi", glavneJedi);
        model.addAttribute("pijaca", pijaca);
        model.addAttribute("sladice", sladice);
        model.addAttribute("dodatki", dodatki);

        return "menu";
    }

    //-------------------------------- Menu funkcije -------------------------------


    public ArrayList<Artikel> posodobiGlavneJedi() {

        ArrayList<Artikel> glavneJedi = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND Zivilo.fk_Tip_Zivila = 1");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                glavneJedi.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return glavneJedi;
    }

    public ArrayList<Artikel> posodobiPijaco() {

        ArrayList<Artikel> pijace = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND Zivilo.fk_Tip_Zivila = 2");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                pijace.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return pijace;
    }

    public ArrayList<Artikel> posodobiSladice() {

        ArrayList<Artikel> sladice = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND Zivilo.fk_Tip_Zivila = 3");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                sladice.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sladice;
    }

    public ArrayList<Artikel> posodobiDodatke() {

        ArrayList<Artikel> dodatki = new ArrayList<>();
        ResultSet rs = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT Zivilo.id_Zivilo as id, Zivilo.naziv as naziv, Tip_Zivila.naziv as tip, Zivilo.slika_Zivila as slika, Zivilo.cena as cena, Zivilo.opis as opis FROM dbo.Zivilo, dbo.Tip_Zivila WHERE Zivilo.fk_Tip_Zivila = Tip_Zivila.id_Tip_Zivila AND (Zivilo.fk_Tip_Zivila = 4 OR Zivilo.fk_Tip_Zivila = 6)");
            while (rs.next()) {
                int id = Integer.parseInt(rs.getString("id"));
                String naziv = rs.getString("naziv");
                String slika = rs.getString("slika");
                String opis = rs.getString("opis");
                String tip = rs.getString("tip");
                Double cena = Double.parseDouble(rs.getString("cena"));
                dodatki.add(new Artikel(id, naziv, opis, tip, slika, 0, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dodatki;
    }
    //------------------------------------------------------------------------


    @RequestMapping(value = {"/product-single"}, method = RequestMethod.GET)
    public String productSingle(Model model) {
        return "product-single";
    }

    @RequestMapping(value = {"/services"}, method = RequestMethod.GET)
    public String services(Model model) {
        return "services";
    }

    @RequestMapping(value = {"/shop"}, method = RequestMethod.GET)
    public String shop(Model model,
                       HttpServletRequest req,
                       HttpServletResponse res) {

        // Potrebni ArrayListi za prikaz različnih živil v ponudbi
        ArrayList<Artikel> glavneJedi = new ArrayList<>();
        ArrayList<Artikel> sladice = new ArrayList<>();
        ArrayList<Artikel> pijaca = new ArrayList<>();
        ArrayList<Artikel> dodatki = new ArrayList<>();

        glavneJedi = posodobiGlavneJedi();
        pijaca = posodobiPijaco();
        sladice = posodobiSladice();
        dodatki = posodobiDodatke();

        model.addAttribute("glavneJedi", glavneJedi);
        model.addAttribute("pijaca", pijaca);
        model.addAttribute("sladice", sladice);
        model.addAttribute("dodatki", dodatki);
        return "shop";
    }

    @RequestMapping(value = {"/posljiNarocilo"}, method = RequestMethod.GET)
    public String posljiNarocilo(HttpSession session, Model model) {
        Connection con = getConnection();
        Narocilo narocilce = (Narocilo) session.getAttribute("narocilo");
        int idUporabnik = (int) session.getAttribute("idUporabnik");
        int index = narocilce.getIdNarocila();
        try {
            PreparedStatement insert = con.prepareStatement("UPDATE Narocilo SET sprejeto = 1 WHERE id_Narocilo = " + index);
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }

        index = novoNarocilo(idUporabnik);
        session.setAttribute("narocilo", new Narocilo(index, idUporabnik));
        session.setAttribute("stArtiklov", 0);

        return "index";
    }

    @RequestMapping(value = {"/potrdiIzbiro"}, method = RequestMethod.GET)
    public String potrdiIzbiro(HttpSession session, Model model) {

        Narocilo narocilce = (Narocilo) session.getAttribute("narocilo");
        SestaviTaco x = (SestaviTaco) session.getAttribute("sestaviTaco");

        String opis = x.vrniOpis();

        Connection con = getConnection();

        try {
            PreparedStatement insert = con.prepareStatement("INSERT INTO Zivilo(naziv,opis,cena,slika_Zivila,fk_Tip_Zivila) VALUES('Sestavljen Taco','" + opis + "',5,'https://banner2.kisspng.com/20180327/dwe/kisspng-taco-time-burrito-mexican-cuisine-taco-bell-tacos-5aba843db98a98.52417979152217298976.jpg',7);");
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }
        ResultSet rs = null;
        int idZivila = 0;
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT MAX(id_Zivilo) as id_Zivilo FROM Zivilo WHERE opis = '" + opis + "'");
            while (rs.next()) {
                idZivila = rs.getInt("id_Zivilo");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        narocilce.dodajArtikel(idZivila, opis, opis, 5, "https://banner2.kisspng.com/20180327/dwe/kisspng-taco-time-burrito-mexican-cuisine-taco-bell-tacos-5aba843db98a98.52417979152217298976.jpg", "7", 1);

        int idUporabnik = (int) session.getAttribute("idUporabnik");
        SestaviTaco novi = new SestaviTaco(idUporabnik);
        session.setAttribute("sestaviTaco", novi);

        int indexNarocila = narocilce.getIdNarocila();
        try {
            PreparedStatement insert = con.prepareStatement("INSERT INTO Narocilo_Zivilo (kolicina,fk_Zivilo,fk_Narocilo) VALUES(" + 1 + "," + idZivila + "," + indexNarocila + ");");
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }

        ArrayList<Artikel> izbraniArtikli = narocilce.getArtikli();
        model.addAttribute("izbraniArtikli", izbraniArtikli);

        session.setAttribute("stArtiklov", narocilce.getArtikli().size());
        double skupniZnesek = 0;
        for (Artikel a : izbraniArtikli) {
            skupniZnesek = skupniZnesek + (a.getKolicina() * a.getCena());
        }
        model.addAttribute("skupniZnesek", skupniZnesek);
        session.setAttribute("skupniZnesek", skupniZnesek);

        return "cart";
    }

    @RequestMapping(value = {"/odstraniIzKosarice"}, method = RequestMethod.GET)
    public String odstraniIzKosarice(HttpSession session, Model model, int id) {
        Narocilo narocilce = (Narocilo) session.getAttribute("narocilo");
        ArrayList<Artikel> artikli = narocilce.getArtikli();
        int idartikla = artikli.get(id).getId();
        int kolicinaartikla = artikli.get(id).getKolicina();

        narocilce.odstraniArtikel(id);
        ArrayList<Artikel> izbraniArtikli = narocilce.getArtikli();
        model.addAttribute("izbraniArtikli", izbraniArtikli);

        session.setAttribute("stArtiklov", narocilce.getArtikli().size());

        int indexNarocila = narocilce.getIdNarocila();
        try {
            PreparedStatement insert = con.prepareStatement("DELETE FROM Narocilo_Zivilo WHERE fk_Narocilo = " + indexNarocila + " AND kolicina = " + kolicinaartikla + " AND fk_Zivilo = " + idartikla);
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }
        return "cart";
    }


    @RequestMapping(value = {"/dodajVKosarico"}, method = RequestMethod.GET)
    public String dodajVkosarico(HttpSession session, int id, int kolicina, Model model) {

        Narocilo narocilce = (Narocilo) session.getAttribute("narocilo");

        ResultSet rs = null;
        Connection con = getConnection();
        int idZivilo = 0;
        String naziv = "";
        String opis = "";
        double cena = 0;
        String urlSlika = "";
        String tipZivila = "";
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT * FROM Zivilo WHERE id_Zivilo = '" + id + "'");
            while (rs.next()) {
                idZivilo = rs.getInt("id_Zivilo");
                naziv = rs.getString("naziv");
                opis = rs.getString("opis");
                cena = rs.getDouble("cena");
                urlSlika = rs.getString("slika_Zivila");
                tipZivila = rs.getString("fk_Tip_Zivila");

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        narocilce.dodajArtikel(idZivilo, naziv, opis, cena, urlSlika, tipZivila, kolicina);
        ArrayList<Artikel> glavneJedi = new ArrayList<>();
        ArrayList<Artikel> sladice = new ArrayList<>();
        ArrayList<Artikel> pijaca = new ArrayList<>();
        ArrayList<Artikel> dodatki = new ArrayList<>();

        glavneJedi = posodobiGlavneJedi();
        pijaca = posodobiPijaco();
        sladice = posodobiSladice();

        dodatki = posodobiDodatke();

        dodatki = posodobiDodatke();
        int indexNarocila = narocilce.getIdNarocila();
        try {
            PreparedStatement insert = con.prepareStatement("INSERT INTO Narocilo_Zivilo (kolicina,fk_Zivilo,fk_Narocilo) VALUES(" + kolicina + "," + id + "," + indexNarocila + ");");
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }

        session.setAttribute("stArtiklov", narocilce.getArtikli().size());

        model.addAttribute("glavneJedi", glavneJedi);
        model.addAttribute("pijaca", pijaca);
        model.addAttribute("sladice", sladice);
        model.addAttribute("dodatki", dodatki);
        return "shop";
    }

    /*
     *   Metoda, ki ustvari novo naročilo z "default vrednostimi"
     *   Vrne index zadnjega naročila (Upajmo, da vedno vrne pravega)
     * */
    public int novoNarocilo(int idUporabnik) {

        ResultSet rs = null;
        Connection con = getConnection();

        try {
            PreparedStatement insert = con.prepareStatement("INSERT INTO Narocilo (skupna_Cena, datum, sprejeto, fk_Uporabnik, fk_Vrsta_Placila) VALUES(0, GETDATE(), 0 ,'" + idUporabnik + "', 1)");
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }

        int index = 0;
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT MAX(id_Narocilo) as id FROM Narocilo");

            while (rs.next()) {
                index = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("index novoNarocilo: " + index);
        return index;
    }

    /*
     *   Metoda, ki ustvari novo naročilo_živilo z "trenutnimi vrednostimo"
     *   Vrne index zadnjega naročila (Upajmo, da vedno vrne pravega)
     * */
    public int novoNarociloZivilo(int idZivilo, int kolicina, int idNarocilo) {

        ResultSet rs = null;
        Connection con = getConnection();

        try {
            PreparedStatement insert = con.prepareStatement("INSERT INTO Narocilo_Zivilo (kolicina, fk_Zivilo, fk_Narocilo) VALUES ('" + kolicina + "','" + idZivilo + "','" + idNarocilo + "')");
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }

        int index = 0;
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT MAX(id_Narocilo_Zivilo) as id FROM Narocilo_Zivilo");
            while (rs.next()) {
                index = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("index novoNarociloZivilo: " + index);
        return index;
    }

    public ArrayList<Artikel> posodobiTrenutnoNarocilo(int id_Narocilo) {

        ArrayList<Artikel> trenutnaNarocila = new ArrayList<>();
        ResultSet res = null;
        Connection con = getConnection();

        try {
            Statement select = con.createStatement();
            res = select.executeQuery("SELECT Zivilo.id_Zivilo as id_Zivilo, Zivilo.naziv as naziv, Zivilo.cena as cena, Zivilo.slika_Zivila as slika, Narocilo_Zivilo.kolicina as kolicina FROM Zivilo, Narocilo_Zivilo WHERE Zivilo.id_Zivilo = Narocilo_Zivilo.fk_Zivilo AND Narocilo_Zivilo.fk_Narocilo =" + id_Narocilo);

            while (res.next()) {
                int id = Integer.parseInt(res.getString("id_Zivilo"));
                String naziv = res.getString("naziv");
                String slika = res.getString("slika");
                int kolicina = Integer.parseInt(res.getString("kolicina"));
                Double cena = Double.parseDouble(res.getString("cena"));
                trenutnaNarocila.add(new Artikel(id, naziv, "", "", slika, kolicina, cena));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return trenutnaNarocila;
    }



    @RequestMapping(value = {"/gmail"}, method = RequestMethod.POST)
    public String gmail( HttpServletRequest request) {
        String ime = request.getParameter("ime");
        String priimek = request.getParameter("priimek");
        String datum = request.getParameter("datum");
        String cas = request.getParameter("cas");
        String telefonska = request.getParameter("telefonska");
        String sporocilo = request.getParameter("sporocilo");



        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setTo("pridina.taco@gmail.com");
            helper.setSubject("Rezervacija mize - Taco.si");

            StringBuilder sb = new StringBuilder();
            sb.append(ime).append(System.lineSeparator());
            sb.append(priimek).append(System.lineSeparator());
            sb.append(datum).append(System.lineSeparator());
            sb.append(cas).append(System.lineSeparator());
            sb.append(telefonska).append(System.lineSeparator());
            sb.append(sporocilo).append(System.lineSeparator());

            helper.setText(sb.toString());



            System.out.println(ime);
            System.out.println(priimek);
            System.out.println(datum);
            System.out.println(cas);
            System.out.println(telefonska);
            System.out.println(sporocilo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        sender.send(message);

        return "index";
    }



    @RequestMapping(value = {"/kontaktiraj"}, method = RequestMethod.POST)
    public String kontaktiraj( HttpServletRequest request) {
        String ime = request.getParameter("ImeinPriimek");
        String email = request.getParameter("email");
        String zadeva = request.getParameter("zadeva");
        String sporocilo = request.getParameter("sporocilo");


        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setTo("pridina.taco@gmail.com");
            helper.setSubject(zadeva);


            StringBuilder sb = new StringBuilder();
            sb.append(ime).append(System.lineSeparator());
            sb.append(email).append(System.lineSeparator());
            sb.append(zadeva).append(System.lineSeparator());
            sb.append(sporocilo).append(System.lineSeparator());

            helper.setText(sb.toString());

            System.out.println(ime);
            System.out.println(email);
            System.out.println(zadeva);
            System.out.println(sporocilo);

        } catch (Exception e) {
            e.printStackTrace();
        }
        sender.send(message);

        return "contact";
    }


    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    @RequestMapping(value = {"/registracija"}, method = RequestMethod.GET)
    public String registracija(Model model) {

        return "registracija";
    }

    @RequestMapping(value = {"/admin"}, method = RequestMethod.GET)
    public String admin(Model model) {
        ArrayList<Zivilo> zivila = posodobiAdmin();
        model.addAttribute("zivila", zivila);
        return "admin";
    }

    @RequestMapping(value = {"/dodajZivilo"}, method = RequestMethod.GET)
    public String admin(Model model, String naziv, double cena, String link, String opis, int id) {
        Connection con = getConnection();
        try {
            PreparedStatement insert = con.prepareStatement("INSERT INTO Zivilo(naziv,opis,cena,slika_Zivila,fk_Tip_Zivila)VALUES ('" + naziv + "','" + opis + "'," + cena + ",'" + link + "'," + id + ")");
            insert.executeUpdate();
        } catch (Exception dodajZivilo) {
            dodajZivilo.printStackTrace();
        }

        ArrayList<Zivilo> zivila = posodobiAdmin();
        model.addAttribute("zivila", zivila);
        return "admin";
    }

    @RequestMapping(value = {"/odstraniZivilo"}, method = RequestMethod.GET)
    public String admin(Model model, int index) {
        Connection con = getConnection();
        try {
            PreparedStatement insert = con.prepareStatement("DELETE FROM Zivilo where id_Zivilo = " + index);
            insert.executeUpdate();
        } catch (Exception updateNarocilo) {
            updateNarocilo.printStackTrace();
        }
        ArrayList<Zivilo> zivila = posodobiAdmin();
        model.addAttribute("zivila", zivila);
        return "admin";
    }

    @RequestMapping(value = {"/narocila"}, method = RequestMethod.GET)
    public String narocila(Model model) {

        ArrayList<PrikazNarocil> nesprejetaNarocila = new ArrayList<PrikazNarocil>();
        ArrayList<PrikazNarocil> sprejetaNarocila = new ArrayList<PrikazNarocil>();

        nesprejetaNarocila = posodobiNesprejetaNarocila();
        sprejetaNarocila = posodobiSprejetaNarocila();

        model.addAttribute("nesprejetaNarocila", nesprejetaNarocila);
        model.addAttribute("sprejetaNarocila", sprejetaNarocila);
        return "narocila";
    }

    @RequestMapping(value = {"/potrdiNarocilo"}, method = RequestMethod.GET)
    public String potrdiNarocilo(Model model, int index) {
        Connection con = getConnection();
        ArrayList<PrikazNarocil> nesprejetaNarocila = new ArrayList<PrikazNarocil>();
        ArrayList<PrikazNarocil> sprejetaNarocila = new ArrayList<PrikazNarocil>();
        try {
            PreparedStatement insert = con.prepareStatement("UPDATE Narocilo SET sprejeto = 2 WHERE id_Narocilo = " + index);
            insert.executeUpdate();
        } catch (Exception updateNarocilo) {
            updateNarocilo.printStackTrace();
        }
        nesprejetaNarocila = posodobiNesprejetaNarocila();
        sprejetaNarocila = posodobiSprejetaNarocila();
        model.addAttribute("nesprejetaNarocila", nesprejetaNarocila);
        model.addAttribute("sprejetaNarocila", sprejetaNarocila);
        posljiSporocilo("Vaše naročilo je potrjeno! LP. Taco", index);

        return "narocila";
    }


    @RequestMapping(value = {"/zavrniNarocilo"}, method = RequestMethod.GET)
    public String zavrniNarocilo(Model model, int index) {
        Connection con = getConnection();
        ArrayList<PrikazNarocil> nesprejetaNarocila = new ArrayList<PrikazNarocil>();
        ArrayList<PrikazNarocil> sprejetaNarocila = new ArrayList<PrikazNarocil>();
        posljiSporocilo("Vaše naročilo je zavrnjeno! LP. Taco", index);

        try {
            PreparedStatement insert = con.prepareStatement("DELETE FROM Narocilo_Zivilo WHERE fk_Narocilo =" + index);
            insert.executeUpdate();
            insert = con.prepareStatement("DELETE FROM Narocilo WHERE id_Narocilo = " + index);
            insert.executeUpdate();
        } catch (Exception deleteNarocilo) {
            deleteNarocilo.printStackTrace();
        }
        nesprejetaNarocila = posodobiNesprejetaNarocila();
        sprejetaNarocila = posodobiSprejetaNarocila();
        model.addAttribute("nesprejetaNarocila", nesprejetaNarocila);
        model.addAttribute("sprejetaNarocila", sprejetaNarocila);


        return "narocila";
    }



    /**
     * Metoda, ki se izvede ko uporabnik vnese vsa potrebna polja in pritisne na gumb za registracijo.
     *
     * @return V primeru da je registracija uspešna, uporabnika avtomatsko prijavi in ga preusmeri na domačo stran, v nasprotnem primeru pa zahteva ponoven vnos podatkov.
     */
    @RequestMapping(value = {"/registracijaUporabnika"}, method = RequestMethod.POST)
    public String registracijaUporabnika(Model model, HttpSession session, String name, String surname, String username, String password, String passwordRepeat, String date, String adress, String city, String postNumber, String telephone, String email) {
        String napakaSporocilo = "";
        boolean check = true;
        ResultSet rs = null;
        //klic metode za pridobitev povezave
        Connection con = getConnection();

        if (!password.equals(passwordRepeat)) {
            check = false;
            napakaSporocilo = napakaSporocilo + "Gesli se ne ujemata! ";
        }

        try {
            //preverjanje, če uporbnik z tem uporabniškim imenom ali elektronsko pošto že obstaja
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT uporabnisko_Ime,elektronska_Posta FROM Uporabnik");
            while (rs.next()) {
                String uporabniskoIme = rs.getString("uporabnisko_Ime");
                String elektronskaPosta = rs.getString("elektronska_Posta");
                if (uporabniskoIme.equals(username)) {
                    check = false;
                    napakaSporocilo = napakaSporocilo + "Uporabnik z tem uporabniškim imenom že obstaja! ";
                }
                if (elektronskaPosta.equals(email)) {
                    check = false;
                    napakaSporocilo = napakaSporocilo + "Uporabnik z tem elektronskim naslovom že obstaja! ";
                }
            }
            boolean preveriPosto = false;
            if (check) {
                //preverjanje ali pošta ter pripadajoča poštna številka že obstaja.
                rs = select.executeQuery("SELECT postna_Stevilka FROM Posta");
                while (rs.next()) {
                    String postnaStevilka = rs.getString("postna_Stevilka");
                    if (postnaStevilka.equals(postNumber)) {
                        preveriPosto = true;
                        break;
                    }
                }
                if (!preveriPosto) {
                    // v primeru da te pošte še ni v PB, se ta vnese
                    PreparedStatement insert = con.prepareStatement("INSERT INTO Posta(kraj,postna_Stevilka)VALUES('" + city + "'," + postNumber + ")");
                    insert.executeUpdate();
                }
                rs = select.executeQuery("SELECT id_Posta FROM Posta WHERE postna_Stevilka='" + postNumber + "'");
                int idPosta = 0;
                while (rs.next()) {
                    idPosta = rs.getInt("id_Posta");
                }
                //vnos uporabnikovega naslova
                PreparedStatement insert = con.prepareStatement("INSERT INTO Naslov(naslov,fk_Posta)VALUES('" + adress + "'," + idPosta + ")");
                insert.executeUpdate();

                rs = select.executeQuery("SELECT DISTINCT id_Naslov FROM Naslov WHERE naslov = '" + adress + "';");
                int idNaslov = 0;
                while (rs.next()) {
                    idNaslov = rs.getInt("id_Naslov");
                }
                //vnos informacij o uporabniku
                username.toLowerCase();

                String kriptiranoGeslo = BCrypt.hashpw(password, BCrypt.gensalt(12));
                insert = con.prepareStatement("SET DATEFORMAT dmy INSERT INTO Uporabnik(ime,priimek,datum_Rojstva,telefonska_Stevilka,elektronska_Posta,uporabnisko_Ime,geslo,vloga,fk_Naslov) VALUES('" + name + "','" + surname + "','" + date + "','" + telephone + "','" + email + "','" + username + "','" + kriptiranoGeslo + "',1," + idNaslov + ")");
                insert.executeUpdate();

                rs = select.executeQuery("SELECT id_Uporabnik FROM Uporabnik where uporabnisko_Ime = '" + username + "'");
                int idUporabnik = 0;
                while (rs.next()) {
                    idUporabnik = rs.getInt("id_Uporabnik");
                }

                int indexNarocila = novoNarocilo(idUporabnik);
                session.setAttribute("narocilo", new Narocilo(indexNarocila, idUporabnik));
                session.setAttribute("sestaviTaco", new SestaviTaco(idUporabnik));
                session.setAttribute("idUporabnik", idUporabnik);
                session.setAttribute("uporabnik", username);
                session.setAttribute("status", 1);
                model.addAttribute("check", check);
                return "index";

            } else {

                model.addAttribute("napaka", napakaSporocilo);
                model.addAttribute("check", check);
                model.addAttribute("ime", name);
                model.addAttribute("priimek", surname);
                model.addAttribute("uporabniskoIme", username);
                model.addAttribute("geslo", password);
                model.addAttribute("ponoviGeslo", passwordRepeat);
                model.addAttribute("datum", date);
                model.addAttribute("naslov", adress);
                model.addAttribute("kraj", city);
                model.addAttribute("posta", postNumber);
                model.addAttribute("telefon", telephone);
                model.addAttribute("eposta", email);
                return "registracija";
            }


        } catch (Exception sqlselect) {
            sqlselect.printStackTrace();
            napakaSporocilo = napakaSporocilo + "Napaka pri registraciji! ";
            model.addAttribute("napaka", napakaSporocilo);
            check = false;
            model.addAttribute("check", check);
            return "registracija";
        }
    }


    /**
     * Metoda, ki se izvede ko uporabnik vnese vsa potrebna polja in pritisne na gumb za prijavo.
     *
     * @return z uspešno prijavo preusmeri uporabnika na domačo stran in ga prijavi, drugače zahteva ponoven vnos podatkov.
     */
    @RequestMapping(value = {"/prijavaUporabnika"}, method = RequestMethod.POST)
    public String prijava(Model model, HttpSession session, String username, String password) {
        ResultSet rs = null;
        String napakaSporocilo = "";
        username.toLowerCase();
        boolean check = true;
        boolean obstaja = false;
        Connection con = getConnection();
        try {
            //preveri če vnešeno uporabniško ime obstaja(če je uporabnik registriran)
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT uporabnisko_Ime FROM Uporabnik");
            while (rs.next()) {
                String uporabniskoIme = rs.getString("uporabnisko_Ime");
                if (uporabniskoIme.equals(username)) {
                    obstaja = true;
                    break;
                }
            }
            if (!obstaja) {
                check = false;
                napakaSporocilo = napakaSporocilo + "Uporabnik ne obstaja!";
                model.addAttribute("napaka", napakaSporocilo);
                model.addAttribute("check", check);
                return "prijava";
            } else {
                //preveri če se geslo pripadajoče temu uporabniškemu imenu ujema z vnešenim geslom.
                rs = select.executeQuery("SELECT geslo, vloga, id_Uporabnik FROM Uporabnik WHERE uporabnisko_Ime = '" + username + "'");
                String gesloUporabnika = "";
                int idUporabnik = -1;
                int indexNarocila = -1;

                int status = 0;
                while (rs.next()) {
                    gesloUporabnika = rs.getString("geslo");
                    status = rs.getInt("vloga");
                    idUporabnik = rs.getInt("id_Uporabnik");
                }
                if (BCrypt.checkpw(password, gesloUporabnika)) {
                    check = true;
                    model.addAttribute("status", status);
                    model.addAttribute("uporabnik", username);
                    session.setAttribute("uporabnik", username);
                    session.setAttribute("status", status);

                    indexNarocila = novoNarocilo(idUporabnik);

                    session.setAttribute("stArtiklov", 0);
                    session.setAttribute("narocilo", new Narocilo(indexNarocila, idUporabnik));

                    session.setAttribute("sestaviTaco", new SestaviTaco(idUporabnik));
                    session.setAttribute("idUporabnik", idUporabnik);
                    return "index";
                } else {
                    check = false;
                    napakaSporocilo = napakaSporocilo + "Geslo ni pravilno! ";
                    model.addAttribute("napaka", napakaSporocilo);
                    model.addAttribute("check", check);
                    return "prijava";
                }
            }
        } catch (Exception prijavaUporabnika) {
            prijavaUporabnika.printStackTrace();
            napakaSporocilo = napakaSporocilo + "Napaka pri prijavi! ";
            model.addAttribute("napaka", napakaSporocilo);
        }
        return "prijava";
    }


    @RequestMapping(value = {"/prijava"}, method = RequestMethod.GET)
    public String prijava(Model model) {
        return "prijava";
    }

    @RequestMapping(value = {"/pozabljenoGeslo"}, method = RequestMethod.GET)
    public String pozabljenoGeslo(Model model) {
        return "pozabljenoGeslo";
    }

    public ArrayList posodobiSprejetaNarocila() {
        ResultSet rs = null;
        ResultSet rs2 = null;
        ArrayList<PrikazNarocil> sprejetaNarocila = new ArrayList<PrikazNarocil>();
        Connection con = getConnection();
        try {
            Statement select = con.createStatement();
            Statement select2 = con.createStatement();
            rs = select.executeQuery("SELECT id_Narocilo,fk_Uporabnik, skupna_Cena, datum, ns.naslov, p.kraj FROM Narocilo n JOIN Uporabnik u ON n.fk_Uporabnik = u.id_Uporabnik JOIN Naslov ns ON u.fk_Naslov = ns.id_Naslov JOIN Posta p ON NS.fk_Posta = p.id_Posta WHERE sprejeto = 2");
            while (rs.next()) {
                int idNarocilo = rs.getInt("id_Narocilo");
                String datum = rs.getString("datum");
                String naslov = rs.getString("naslov");
                String kraj = rs.getString("kraj");
                int idUporabnik = rs.getInt("fk_Uporabnik");
                String opis = "";
                double cena = 0;
                rs2 = select2.executeQuery("SELECT z.naziv, z.cena FROM Zivilo z JOIN Narocilo_Zivilo nz ON z.id_Zivilo = nz.fk_Zivilo JOIN Narocilo n ON nz.fk_Narocilo = n.id_Narocilo WHERE id_Narocilo = " + idNarocilo);
                while (rs2.next()) {
                    opis = opis + rs2.getString("naziv") + ",";
                    cena = cena + rs2.getDouble("cena");
                }
                sprejetaNarocila.add(new PrikazNarocil(idNarocilo, datum, naslov, kraj, opis, cena, idUporabnik));
            }
        } catch (Exception sprejeteNarocile) {
            sprejeteNarocile.printStackTrace();
        }
        return sprejetaNarocila;
    }

    public ArrayList posodobiNesprejetaNarocila() {
        ResultSet rs = null;
        ResultSet rs2 = null;
        ArrayList<PrikazNarocil> nesprejetaNarocila = new ArrayList<PrikazNarocil>();
        Connection con = getConnection();
        try {
            Statement select = con.createStatement();
            Statement select2 = con.createStatement();
            rs = select.executeQuery("SELECT id_Narocilo,fk_Uporabnik, skupna_Cena, datum, ns.naslov, p.kraj FROM Narocilo n JOIN Uporabnik u ON n.fk_Uporabnik = u.id_Uporabnik JOIN Naslov ns ON u.fk_Naslov = ns.id_Naslov JOIN Posta p ON NS.fk_Posta = p.id_Posta WHERE sprejeto = 1");
            while (rs.next()) {
                int idNarocilo = rs.getInt("id_Narocilo");
                String datum = rs.getString("datum");
                String naslov = rs.getString("naslov");
                String kraj = rs.getString("kraj");
                int idUporabnik = rs.getInt("fk_Uporabnik");
                String opis = "";
                String opis2 = "";
                double cena = 0;
                rs2 = select2.executeQuery("SELECT z.naziv, z.cena, z.opis, z.fk_Tip_Zivila FROM Zivilo z JOIN Narocilo_Zivilo nz ON z.id_Zivilo = nz.fk_Zivilo JOIN Narocilo n ON nz.fk_Narocilo = n.id_Narocilo WHERE id_Narocilo = " + idNarocilo);
                while (rs2.next()) {
                    int tip = rs2.getInt("fk_Tip_Zivila");
                    opis2 = rs2.getString("opis");
                    if (tip == 7) {
                        opis = opis + rs2.getString("naziv") + "(" + opis2 + "), ";
                    } else {
                        opis = opis + rs2.getString("naziv") + ", ";
                    }
                    cena = cena + rs2.getDouble("cena");
                }
                nesprejetaNarocila.add(new PrikazNarocil(idNarocilo, datum, naslov, kraj, opis, cena, idUporabnik));
            }
        } catch (Exception nesprejeteNarocile) {
            nesprejeteNarocile.printStackTrace();
        }
        return nesprejetaNarocila;
    }

    public ArrayList posodobiAdmin() {
        ResultSet rs = null;
        ArrayList<Zivilo> zivila = new ArrayList<Zivilo>();
        Connection con = getConnection();
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT id_Zivilo, naziv, slika_Zivila, cena FROM Zivilo");
            while (rs.next()) {
                int idZivilo = rs.getInt("id_Zivilo");
                String naziv = rs.getString("naziv");
                String slikaZivila = rs.getString("slika_Zivila");
                double cena = rs.getDouble("cena");
                zivila.add(new Zivilo(idZivilo, naziv, slikaZivila, cena));
            }
        } catch (Exception posodobiAdmin) {
            posodobiAdmin.printStackTrace();
        }
        return zivila;
    }


    public void posljiSporocilo(String sporocilo, int index) {
        Connection con = getConnection();
        ResultSet rs;
        String elektronskaPosta = "";
        try {
            Statement select = con.createStatement();
            rs = select.executeQuery("SELECT elektronska_Posta FROM Uporabnik u JOIN Narocilo n ON u.id_Uporabnik = n.fk_Uporabnik WHERE id_Narocilo = " + index);
            while (rs.next()) {
                elektronskaPosta = rs.getString("elektronska_Posta");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MimeMessage message = sender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(message);
        try {
            helper.setTo(elektronskaPosta);
            helper.setText(sporocilo);
            helper.setSubject("Naročilo Taco");
        } catch (Exception e) {
            e.printStackTrace();
        }
        sender.send(message);
    }

    // Metoda za prenos XML datoteke

    @Test
    public void whenJavaSerializedToXmlFile_thenCorrect(HttpSession session, String filePath) throws IOException {

        Narocilo narocilo = (Narocilo) session.getAttribute("narocilo");

        XmlMapper xmlMapper = new XmlMapper();
        String xml = xmlMapper.writeValueAsString(narocilo);
        xmlMapper.writeValue(new File(filePath), narocilo);
        File file = new File(filePath);
        assertNotNull(file);
        System.out.println(xml);
    }

    // BRANJE XML

    @Test
    public Narocilo whenJavaGotFromXmlFile_thenCorrect(String filePath) throws IOException {
        File file = new File(filePath);

        XmlMapper xmlMapper = new XmlMapper();
        String xml = inputStreamToString(new FileInputStream(file));
        System.out.println(xml);

        Narocilo value = xmlMapper.readValue(xml, Narocilo.class);
        System.out.println(value);
        return value;
    }

    public String inputStreamToString(InputStream is) throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
            sb.append(line);
        }
        br.close();
        is.close();
        return sb.toString();
    }
}
/*
------------------- UPORABA PODATKOVNE BAZE -------------------
  Connection con = getConnection();  // da pridobimo povezavo iz metode "getConnection()", ki jo vzpostavi
  BRANJE IZ PODTAKOVNE BAZE
    - Statement select = con.createStatement(); // z pomočjo connectiona (con) ustvarimo statement za branje iz PB (MORA BITI STATEMENT)
    - ResultSet rs = null; //deklariramo ResultSet, kamor bomo shranjevali rezultate iz PB
    - rs = select.executeQuery("POIZVEDBA"); // v resultset shranimo poizvedbo
    - while (rs.next()) {
        gesloUporabnika = rs.getString("geslo");  //nato iz resultseta beremo vrstico po vrstico (atribute shranjujemo z getString, getInt itd.
        }                                         //v oklepaju za getString() mora biti ime vrstice, ki jo želimo shraniti, katero smo dobili kot rezultat iz PB poizvedbe
  PISANJE V PODTAKOVNO BAZO
    - PreparedStatement insert = con.prepareStatement(INSERT ALI DRUG UKAZ); // z pomočjo connectiona (con) ustvarimo PreparedStatement za PISANJE v PB (MORA BITI PreparedStatement)
    - insert.executeUpdate(); preparedStatement moramo posebej zagnat z executeUpdate(), navadnega statementa z select ne rabmo.

 */
