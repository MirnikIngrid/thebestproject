package com.praktikum.taco.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SestaviTaco implements Serializable {
    int idUporabnika;
    ArrayList<String> nazivi = new ArrayList<String>();

    public SestaviTaco(int idUporabnika){
        this.idUporabnika=idUporabnika;
    }

    public void dodajSestavino(String naziv){
        if(!nazivi.contains(naziv)){
            nazivi.add(naziv);
        }
    }

    public String vrniOpis(){
        String opis ="";
        for(int x = 0; x<nazivi.size();x++){
            opis = opis + nazivi.get(x)+", ";
        }
        if (opis != null && opis.length() > 0 && opis.charAt(opis.length() - 1) == ',') {
            opis = opis.substring(0, opis.length() - 1);
        }
        return opis;
    }

    public void odstraniSestavino(String naziv){
        nazivi.remove(naziv);
    }

    public ArrayList getNazivi() {
        return nazivi;
    }

    public void setNazivi(ArrayList indexi) {
        this.nazivi = indexi;
    }

    public int getIdUporabnika() {
        return idUporabnika;
    }

    public void setIdUporabnika(int idUporabnika) {
        this.idUporabnika = idUporabnika;
    }
}
