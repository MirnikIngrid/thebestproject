package com.praktikum.taco.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Narocilo implements Serializable {
    int idNarocila;
    int idUporabnika;
    ArrayList<Artikel> artikli = new ArrayList<Artikel>();


    public Narocilo () {

    }


    public Narocilo(int idNarocila, int idUporabnika){
        this.idNarocila = idNarocila;
        this.idUporabnika = idUporabnika;
    }

    public void dodajArtikel(int idZivila, String naziv, String opis, double cena, String urlSlika, String tipZivila, int kolicina){
            artikli.add(new Artikel(idZivila,naziv,opis,tipZivila,urlSlika,kolicina,cena));
    }

    public void odstraniArtikel(int index){
        artikli.remove(index);
    }

    public ArrayList<Artikel> getArtikli() {
        return artikli;
    }

    public void setArtikli(ArrayList<Artikel> artikli) {
        this.artikli = artikli;
    }

    public int getIdUporabnika() {
        return idUporabnika;
    }

    public void setIdUporabnika(int idUporabnika) {
        this.idUporabnika = idUporabnika;
    }

    public int getIdNarocila() {
        return idNarocila;
    }

    public void setIdNarocila(int idNarocila) {
        this.idNarocila = idNarocila;
    }




}
