package com.praktikum.taco.model;

import java.awt.*;
import java.io.Serializable;


/**
 *  Razred se uporablja za lažji prikaz živil/sestavin/artiklov na strani
 * */
public class Artikel implements Serializable {

    private int id;
    private String naziv;
    private String opis;
    private String tip;
    private String urlImg;
    private int kolicina;
    private double cena;

    public Artikel() {}

    public Artikel(int id, String naziv, String opis, String tip, String urlImg, int kolicina, double cena) {
        this.id = id;
        this.naziv = naziv;
        this.opis = opis;
        this.tip = tip;
        this.urlImg = urlImg;
        this.kolicina = kolicina;
        this.cena = cena;
    }

    // ------------------ GET/SET -------------------------------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public int getKolicina() {
        return kolicina;
    }

    public void setKolicina(int kolicina) {
        this.kolicina = kolicina;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    //--------------- toString --------------------

    @Override
    public String toString() {
        return "Artikel{" +
                "id=" + id +
                ", naziv='" + naziv + '\'' +
                ", opis='" + opis + '\'' +
                ", tip='" + tip + '\'' +
                ", urlImg='" + urlImg + '\'' +
                ", kolicina=" + kolicina +
                ", cena=" + cena +
                "}\n";
    }
}
