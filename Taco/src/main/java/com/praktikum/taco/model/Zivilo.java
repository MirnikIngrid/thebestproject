package com.praktikum.taco.model;

import java.awt.*;
import java.io.Serializable;

public class Zivilo implements Serializable {

    private int id;
    private String naziv;
    private String urlImg;
    private double cena;

    public Zivilo() {}

    public Zivilo(int id, String naziv, String urlImg, double cena) {
        this.id = id;
        this.naziv = naziv;
        this.urlImg = urlImg;
        this.cena = cena;
    }


    // ------------------ GET/SET -------------------------------


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public double getCena() {
        return cena;
    }

    public void setCena(double cena) {
        this.cena = cena;
    }
}
