<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="sl">
<head>
	<%@ page contentType="text/html;charset=UTF-8" %>

	<title>Taco.si</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<script src="https://cdn.jsdelivr.net/npm/jquery@3.2.1/dist/jquery.min.js" ></script>
	<script src="js/mail.js"></script>


	<link rel="stylesheet" href="${pageContext.request.contextPath}css/open-iconic-bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}css/animate.css">

	<link rel="stylesheet" href="${pageContext.request.contextPath}css/owl.carousel.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}css/owl.theme.default.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}css/magnific-popup.css">

	<link rel="stylesheet" href="${pageContext.request.contextPath}css/aos.css">

	<link rel="stylesheet" href="${pageContext.request.contextPath}css/ionicons.min.css">

	<link rel="stylesheet" href="${pageContext.request.contextPath}css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}css/jquery.timepicker.css">


	<link rel="stylesheet" href="${pageContext.request.contextPath}css/flaticon.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}css/icomoon.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}css/style.css">





</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
	<div class="container">
		<a class="navbar-brand" href="${pageContext.request.contextPath}index">Taco<!--<small>Blend</small>--></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="oi oi-menu"></span> Menu
		</button>
		<div class="collapse navbar-collapse" id="ftco-nav">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item active"><a href="${pageContext.request.contextPath}index" class="nav-link">Domov</a></li>
				<li class="nav-item"><a href="${pageContext.request.contextPath}menu" class="nav-link">Meni</a></li>
				<li class="nav-item"><a href="${pageContext.request.contextPath}contact" class="nav-link">Kontakt</a></li>

				<c:choose>
					<c:when test="${sessionScope.status == 1 || sessionScope.status == 2}">
						<li class="nav-item"><a href="${pageContext.request.contextPath}blog" class="nav-link">Sestavi</a></li>

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${pageContext.request.contextPath}room" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trgovina</a>
					<div class="dropdown-menu" aria-labelledby="dropdown04">
						<a class="dropdown-item" href="${pageContext.request.contextPath}shop">E-Trgovina</a>
						<a class="dropdown-item" href="${pageContext.request.contextPath}cart">Košarica</a>
						<a class="dropdown-item" href="${pageContext.request.contextPath}checkout">Račun</a>
					</div>
				</li>
				<li class="nav-item cart"><a href="${pageContext.request.contextPath}cart" class="nav-link"><span class="icon icon-shopping_cart"></span><span class="bag d-flex justify-content-center align-items-center"><small>${sessionScope.stArtiklov}</small></span></a></li>
					</c:when>
				</c:choose>

				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="${pageContext.request.contextPath}room" id="dropdownPrijava" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-fw fa-user"></i>
						<c:choose>
							<c:when test="${sessionScope.status != 0}">
								${sessionScope.uporabnik}
							</c:when>
						</c:choose>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdown04">
						<c:choose>
							<c:when test="${sessionScope.status != 1 && sessionScope.status != 2}">
								<a class="dropdown-item" href="${pageContext.request.contextPath}prijava">Prijava</a>
								<a class="dropdown-item" href="${pageContext.request.contextPath}registracija">Registracija</a>

							</c:when>
							<c:otherwise>
								<c:choose>
									<c:when test="${sessionScope.status == 2}">
										<a class="dropdown-item" href="${pageContext.request.contextPath}admin">Sestavine</a>
										<a class="dropdown-item" href="${pageContext.request.contextPath}narocila">Naročila</a>
									</c:when>
								</c:choose>
								<a class="dropdown-item" href="${pageContext.request.contextPath}odjava">Odjava</a>
							</c:otherwise>
						</c:choose>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>

<section class="home-slider owl-carousel">
	<div class="slider-item" style="background-image: url(images/taco_slike/cover.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

				<div class="col-md-8 col-sm-12 text-center ftco-animate">
					<span class="subheading">Dobrodošli</span>
					<h1 class="mb-4">Najboljši Taco V Mestu</h1>
					<p class="mb-4 mb-md-5">Povabite prijatelje na pravo mehiško zabavo!</p>
					<p><a href="${pageContext.request.contextPath}shop" class="btn btn-primary p-3 px-xl-4 py-xl-3">Naroči Zdaj</a> <a href="menu" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Poglej Meni</a></p>
				</div>

			</div>
		</div>
	</div>

	<div class="slider-item" style="background-image: url(images/taco_slike/coverPhoto2.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">

				<div class="col-md-8 col-sm-12 text-center ftco-animate">
					<span class="subheading">Dobrodošli</span>
					<h1 class="mb-4">Najboljši Taco V Mestu</h1>
					<p class="mb-4 mb-md-5">Povabite prijatelje na pravo mehiško zabavo!</p>
					<p><a href="#" class="btn btn-primary p-3 px-xl-4 py-xl-3">Naroči Zdaj</a>
						<a href="#" class="btn btn-white btn-outline-white p-3 px-xl-4 py-xl-3">Poglej Meni</a></p>
				</div>

			</div>
		</div>
	</div>
</section>

<section class="ftco-intro">
	<div class="container-wrap">
		<div class="wrap d-md-flex align-items-xl-end">
			<div class="info">
				<div class="row no-gutters">
					<div class="col-md-4 d-flex ftco-animate">
						<div class="icon"><span class="icon-phone"></span></div>
						<div class="text">
							<h3>+386 456 7890</h3>
							<p>Pridi na taco!</p>
						</div>
					</div>
					<div class="col-md-4 d-flex ftco-animate">
						<div class="icon"><span class="icon-my_location"></span></div>
						<div class="text">
							<h3>Koroška cesta 46</h3>
							<p>	2000 Maribor, Slovenija</p>
						</div>
					</div>
					<div class="col-md-4 d-flex ftco-animate">
						<div class="icon"><span class="icon-clock-o"></span></div>
						<div class="text">
							<h3>Odprto Ponedeljek-Sobota</h3>
							<p>8:00 - 21:00</p>
						</div>
					</div>
				</div>
			</div>
			<div class="book p-4">
				<h3>REZERVIRAJ MIZO</h3>
				<div class="status" style="color: white;"></div>
				<!--<form action="https://formspree.io/pridina.taco@gmail.com" method="POST" class="appointment-form" >-->
				<form action="/gmail" method="POST" class="appointment-form" >
					<div class="d-md-flex">
						<div class="form-group">
							<input type="text" id="Ime"  name="ime" class="form-control" minlength="3"required placeholder="Ime">
						</div>
						<div class="form-group ml-md-4">
							<input type="text" id="Priimek" name="priimek" class="form-control" minlength="3" required placeholder="Priimek">
						</div>
					</div>
					<div class="d-md-flex">
						<div class="form-group">
							<div class="input-wrap">
								<div class="icon"><span class="ion-md-calendar"></span></div>
								<input type="text" id="Datum" name="datum" class="form-control appointment_date" required placeholder="Datum">
							</div>
						</div>
						<div class="form-group ml-md-4">
							<div class="input-wrap">
								<div class="icon"><span class="ion-ios-clock"></span></div>
								<input type="text" id="Cas" name="cas" class="form-control appointment_time" required placeholder="Čas">
							</div>
						</div>
						<div class="form-group ml-md-4">
							<input type="text" id="Telefonska" name="telefonska_stevilka" class="form-control" required maxlength="9" placeholder="Telefonska številka">
						</div>
					</div>
					<div class="d-md-flex">
						<div class="form-group">
							<textarea id="Sporocilo" cols="30" rows="2" name="sporocilo" class="form-control "required maxlength="100" placeholder="Sporočilo"></textarea>
						</div>
						<div class="form-group">
							<button type="submit" value="submit" class="btn btn-primary py-3 px-5"/>Pošlji
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>

<section class="ftco-about d-md-flex">
	<div class="one-half img" style="background-image: url(images/taco_slike/restavracija.jpg);"></div>
	<div class="one-half ftco-animate">
		<div class="overlap">
			<div class="heading-section ftco-animate ">
				<span class="subheading">Odkrij</span>
				<h2 class="mb-4">Našo Zgodbo</h2>
			</div>
			<div>
				<p> Taco je več kot restavracija. Je prostor, kjer lahko v miru spijete prvo jutranjo kavo ob vonju našega sveže pečenega kruha in prav tako v naši kuhinji pripravljenih piškotov. Je prostor, kamor povabite svoje poslovne partnerje in je prostor, kjer si skriti pred očmi radovednežev zaljubljeno zrete v oči. Je prostor, kjer se na večerni zabavi srečata študent in njegov profesor, delavec in direktor. Predvsem pa je Taco mehiška restavracija, ki poskuša ujeti duha Mehike v hrani in ambientu in od tod tudi ime restavracije; izvira iz španske besede »taco«, ki pomeni tipično mehiško jed iz pšenične ali koruzne tortilje, polnjene z različnimi nadevi.
					<br> Vstopite in dober tek!</p>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section ftco-services">
	<div class="container">
		<div class="row">
			<div class="col-md-4 ftco-animate">
				<div class="media d-block text-center block-6 services">
					<div class="icon d-flex justify-content-center align-items-center mb-5">
						<span class="flaticon-choices"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Enostavno naročilo</h3>
						<p>Omogočamo ti, da le v parih klikih naročiš svoj edinstven Taco!</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 ftco-animate">
				<div class="media d-block text-center block-6 services">
					<div class="icon d-flex justify-content-center align-items-center mb-5">
						<span class="flaticon-delivery-truck"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Hitra Dostava</h3>
						<p>Potrudili se bomo, da ti v najkrajšem možnem času dostavimo hrano na tvoj naslov.</p>
					</div>
				</div>
			</div>
			<div class="col-md-4 ftco-animate">
				<div class="media d-block text-center block-6 services">
					<div class="icon d-flex justify-content-center align-items-center mb-5">
						<img src="https://img.icons8.com/windows/64/000000/taco.png"></div>
					<div class="media-body">
						<h3 class="heading">Edinstven Taco</h3>
						<p>Želiš ustvariti unikaten taco? Poigraj se z začimbami, sestavinami in ustvari lasten taco.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-6 pr-md-5">
				<div class="heading-section text-md-right ftco-animate">
					<span class="subheading">Odkrij</span>
					<h2 class="mb-4">Naš Meni</h2>
					<p class="mb-4">Izvrstna mehiška hrana v kombinaciji s pristno mehiško pijačo vam je v poletnih mesecih na voljo na čudovitem vrtu, kjer lahko posedite tudi v naših, s slamo pokritih hiškah, na zelenici ob reki Dravi. V zimskih mesecih pa se lahko pogrejete ob odprtem kaminu v sami restavraciji. Prelistaj m</p>
					<p><a href="menu" class="btn btn-primary btn-outline-primary px-4 py-3">Poglej Vse</a></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="row">
					<div class="col-md-6">
						<div class="menu-entry">
							<a href="#" class="img" style="background-image: url(images/taco_slike/tacoSest.jpg);"></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="menu-entry mt-lg-4">
							<a href="#" class="img" style="background-image: url(images/taco_slike/tacoOsem.jpg);"></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="menu-entry">
							<a href="#" class="img" style="background-image: url(images/taco_slike/tacoTri.jpg);"></a>
						</div>
					</div>
					<div class="col-md-6">
						<div class="menu-entry mt-lg-4">
							<a href="#" class="img" style="background-image: url(images/taco_slike/tacoPet.jpg);"></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ftco-counter ftco-bg-dark img" id="section-counter" style="background-image: url(images/taco_slike/tacoPet.jpg);" data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<div class="row">
					<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18 text-center">
							<div class="text">
								<div class="icon"><img src="https://img.icons8.com/metro/64/000000/taco.png"></div>
								<strong class="number" data-number="20">0</strong>
								<span>Število Tacosov</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18 text-center">
							<div class="text">
								<div class="icon"><img src="https://img.icons8.com/metro/64/000000/taco.png"></div>
								<strong class="number" data-number="85">0</strong>
								<span>Osvojene Nagrade</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 d-flex justify-content-center ftco-animate">
						<div class="block-18 text-center">
							<div class="text">
								<div class="icon"><img src="https://img.icons8.com/metro/64/000000/taco.png"></div>

								<strong class="number" id="stevec">0</strong>
								<span>Zadovoljne Stranke</span>
							</div>
						</div>
					</div>
					<div class="col-md-6 col-lg-3 d-flex justify-content-center counter-wrap ftco-animate">
						<div class="block-18 text-center">
							<div class="text">
								<div class="icon"><img src="https://img.icons8.com/metro/64/000000/taco.png"></div>
								<strong class="number" data-number="4">0</strong>
								<span>Zaposleni</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="ftco-section">
	<div class="container">
		<div class="row justify-content-center mb-5 pb-3">
			<div class="col-md-7 heading-section ftco-animate text-center">
				<span class="subheading">Odkrij</span>
				<h2 class="mb-4">Najbolj Prodajane Tacose</h2>
				<p>Preveri katere tacose največkrat naročijo naši gostje.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				<div class="menu-entry">
					<a href="#" class="img" style="background-image: url(images/taco_slike/tacobellStiri.jpg);"></a>
					<div class="text text-center pt-4">
						<h3><a href="#">Kamniški Pekoči Taco</a></h3>
						<p>Pekoči kamniški taco je znan po dvakratni porciji piščančjega mesa z veganskim sirom in domačimi čiliji. Obljubimo, da ne boste odšli lačni od mize! </p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="menu-entry">
					<a href="#" class="img" style="background-image: url(images/taco_slike/tacobellEna.jpg);"></a>
					<div class="text text-center pt-4">
						<h3><a href="#">Šentjurski BG Specialci</a></h3>
						<p>Brez glutenski taco je pri nas prava uspešnica. Mix 10 sestavin popestri nacho sir, dodatno vrednost pa doda domača koruzna tortilija od šefa Bobija.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="menu-entry">
					<a href="#" class="img" style="background-image: url(images/taco_slike/tacobellTri.jpg);"></a>
					<div class="text text-center pt-4">
						<h3>Mladi Radizelčan</h3>
						<p>V tacosu so uporabljene izključno sveže radizelske sestavine. Postrežemo Vam tudi z domačim Radiželčanom, ki se odlično prileže mešanici okusov.</p>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="menu-entry">
					<a href="#" class="img" style="background-image: url(images/taco_slike/tacobellDva.jpg);"></a>
					<div class="text text-center pt-4">
						<h3>Fit Celjanar</h3>
						<p>Največkrat nagrajen taco je prav Fit Celjanar. Skrivni recept od naše vodje Ingrid naredi taco svež in zdrav. Zagotovaljamo, da boste polni energije kar celi teden. </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>


<section class="ftco-section img" id="ftco-testimony" style="background-image: url(images/taco_slike/coverTri.jpg);"  data-stellar-background-ratio="0.5">
	<div class="overlay"></div>
	<div class="container">
		<div class="row justify-content-center mb-5">
			<div class="col-md-7 heading-section text-center ftco-animate">
				<span class="subheading">Mnenje</span>
				<h2 class="mb-4">Strank O Nas</h2>
				<p>Zelo smo veseli vaše kritike, ker samo tako lahko zrastemo in napredujemo.</p>
			</div>
		</div>
	</div>
	<div class="container-wrap">
		<div class="row d-flex no-gutters">
			<div class="col-lg align-self-sm-end ftco-animate">
				<div class="testimony">
					<blockquote>
						<p>&ldquo;Odlična hrana, odlična postrežba, odlična lokacija. V Tacu je vsaka priložnost pravo doživetje, naj bo kosilo med tednom ali sobotna romantična večerja ali praznovanje in zabave!!!&rdquo;</p>
					</blockquote>
					<div class="author d-flex mt-4">
						<div class="image mr-3 align-self-center">
							<img src="images/taco_slike/lucija.jpg" alt="">
						</div>
						<div class="name align-self-center">Lucija Brezočnik  <span class="position">Kritik</span></div>
					</div>
				</div>
			</div>
			<div class="col-lg align-self-sm-end">
				<div class="testimony overlay">
					<blockquote>
						<p>&ldquo;Zelo prijazni in ustrežljivi, hrana odlična in vzdušje fantastično! Radi pridemo v Taco v Mariboru.&rdquo;</p>
					</blockquote>
					<div class="author d-flex mt-4">
						<div class="image mr-3 align-self-center">
							<img src="images/person_2.jpg" alt="">
						</div>
						<div class="name align-self-center">Sašo Karakatič<span class="position">Kritik</span></div>
					</div>
				</div>
			</div>
			<div class="col-lg align-self-sm-end ftco-animate">
				<div class="testimony">
					<blockquote>
						<p>&ldquo;Zahvaljujemo se ekipi Taco za odlično postrežbo in okusno hrano. Hvala za vse &rdquo;</p>
					</blockquote>
					<div class="author d-flex mt-4">
						<div class="image mr-3 align-self-center">
							<img src="images/person_3.jpg" alt="">
						</div>
						<div class="name align-self-center">Alen Rajšp <span class="position">Kritik</span></div>
					</div>
				</div>
			</div>
			<div class="col-lg align-self-sm-end">
				<div class="testimony overlay">
					<blockquote>
						<p>&ldquo;Čista 10!&rdquo;</p>
					</blockquote>
					<div class="author d-flex mt-4">
						<div class="image mr-3 align-self-center">
							<img src="images/taco_slike/lucija.jpg" alt="">
						</div>
						<div class="name align-self-center">Lucija Brezočnik <span class="position">Kritik</span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>



<footer class="ftco-footer ftco-section img">
	<div class="overlay"></div>
	<div class="container">
		<div class="row mb-5">
			<div class="col-lg-4 col-md-6 mb-5 mb-md-5">
				<div class="ftco-footer-widget mb-4">
					<h2 class="ftco-heading-2">O nas</h2>
					<p>Taco je več kot restavracija. Je prostor, kjer lahko v miru spijete prvo jutranjo kavo ob vonju našega sveže pečenega kruha in prav tako v naši kuhinji pripravljenih piškotov.
						Je prostor, kamor povabite svoje poslovne partnerje in je prostor, kjer si skriti pred očmi radovednežev zaljubljeno zrete v oči.</p>
					<ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
						<li class="ftco-animate"><a href="https://twitter.com/tacobell" target="_blank"><span class="icon-twitter"></span></a></li>
						<li class="ftco-animate"><a href="https://www.facebook.com/tacobell/" target="_blank"><span class="icon-facebook"></span></a></li>
						<li class="ftco-animate"><a href="https://www.instagram.com/tacobell/" target="_blank"><span class="icon-instagram"></span></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 mb-5 mb-md-5">
				<div class="ftco-footer-widget mb-4 ml-md-4">
					<h2 class="ftco-heading-2">Postrežba</h2>
					<ul class="list-unstyled">
						<li><a href="#" class="py-2 d-block">Naročila</a></li>
						<li><a href="#" class="py-2 d-block">Dostava hrane</a></li>
						<li><a href="#" class="py-2 d-block">Kvaliteta</a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 mb-5 mb-md-5">
				<div class="ftco-footer-widget mb-4">
					<h2 class="ftco-heading-2">Imaš vprašanje?</h2>
					<div class="block-23 mb-3">
						<ul>
							<li><span class="icon icon-map-marker"></span><span class="text">Koroška cesta 46, 2000 Maribor</span></li>
							<li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
							<li><a href="#"><span class="icon icon-envelope"></span><span class="text">info@taco.si</span></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">

				<p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
					Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
					<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
			</div>
		</div>
	</div>
</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/jquery.timepicker.min.js"></script>
<script src="js/scrollax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlG9MqkKRR9ls27VFVB78QD_avgdrtUVI&sensor=false"></script>
<script src="js/google-map.js"></script>
<script src="js/main.js"></script>

<script>

	//function counter() {
		var i = 10345;
		var element = document.getElementById("stevec");

		var stevec = function() {
			element.innerHTML = i;
			if (i == 40000) clearInterval(this);
			//else console.log('Currently at ' + (i++));
		};
		// This block will be executed 100 times.
		setInterval(stevec, 2000);
		stevec();
	//} // End

</script>
</body>
</html>
