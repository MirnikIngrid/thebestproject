<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="sl">
  <head>
      <%@ page contentType="text/html;charset=UTF-8" %>
    <title>Taco.si</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="css/animate.css">
    
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">

    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/ionicons.min.css">

    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="css/jquery.timepicker.css">

    
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/icomoon.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/stajl.css">

  </head>
  <body>
  <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
      <div class="container">
          <a class="navbar-brand" href="${pageContext.request.contextPath}index">Taco<!--<small>Blend</small>--></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="oi oi-menu"></span> Menu
          </button>
          <div class="collapse navbar-collapse" id="ftco-nav">
              <ul class="navbar-nav ml-auto">
                  <li class="nav-item"><a href="${pageContext.request.contextPath}index" class="nav-link">Domov</a></li>
                  <li class="nav-item"><a href="${pageContext.request.contextPath}menu" class="nav-link">Meni</a></li>
                  <li class="nav-item"><a href="${pageContext.request.contextPath}contact" class="nav-link">Kontakt</a></li>

                  <c:choose>
                      <c:when test="${sessionScope.status == 1 || sessionScope.status == 2}">
                          <li class="nav-item"><a href="${pageContext.request.contextPath}blog" class="nav-link">Sestavi</a></li>

                  <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="${pageContext.request.contextPath}room" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Trgovina</a>
                  <div class="dropdown-menu" aria-labelledby="dropdown04">
                      <a class="dropdown-item" href="${pageContext.request.contextPath}shop">E-Trgovina</a>
                      <a class="dropdown-item" href="${pageContext.request.contextPath}cart">Košarica</a>
                      <a class="dropdown-item" href="${pageContext.request.contextPath}checkout">Račun</a>
                  </div>
              </li>
                  <li class="nav-item cart"><a href="${pageContext.request.contextPath}cart" class="nav-link"><span class="icon icon-shopping_cart"></span><span class="bag d-flex justify-content-center align-items-center"><small>${sessionScope.stArtiklov}</small></span></a></li>
                      </c:when>
                  </c:choose>
                  <li class="nav-item dropdown">
                      <a class="nav-link dropdown-toggle" href="${pageContext.request.contextPath}room" id="dropdownPrijava" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-fw fa-user"></i>
                          <c:choose>
                              <c:when test="${sessionScope.status != 0}">
                                  ${sessionScope.uporabnik}
                              </c:when>
                          </c:choose>
                      </a>
                      <div class="dropdown-menu" aria-labelledby="dropdown04">
                          <c:choose>
                              <c:when test="${sessionScope.status != 1 && sessionScope.status != 2}">
                                  <a class="dropdown-item" href="${pageContext.request.contextPath}prijava">Prijava</a>
                                  <a class="dropdown-item" href="${pageContext.request.contextPath}registracija">Registracija</a>

                              </c:when>
                              <c:otherwise>
                                  <c:choose>
                                      <c:when test="${sessionScope.status == 2}">
                                          <a class="dropdown-item" href="${pageContext.request.contextPath}admin">Sestavine</a>
                                          <a class="dropdown-item" href="${pageContext.request.contextPath}narocila">Naročila</a>
                                      </c:when>
                                  </c:choose>
                                  <a class="dropdown-item" href="${pageContext.request.contextPath}odjava">Odjava</a>
                              </c:otherwise>
                          </c:choose>
                      </div>
                  </li>
              </ul>
          </div>
      </div>
  </nav>
    <!-- END nav -->

      <div data-stellar-background-ratio="0.5">
          <div class="containerpb-3" style="padding-top: 110px; padding-bottom: 110px; opacity: 0.9;">
              <div class="row p-5">
                  <div class="col-md-8 ftco-animate">
                      <form action="#" class="billing-form ftco-bg-dark p-3 p-md-5">
                          <h3 class="mb-4 billing-heading">Podrobnosti plačila</h3>
                          <div class="row align-items-end">
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="ime">Ime</label>
                                      <input id="ime" type="text" class="form-control" value="${ime}">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="priimek">Priimek</label>
                                      <input id="priimek" type="text" class="form-control" value="${priimek}">
                                  </div>
                              </div>
                              <div class="w-100"></div>
                              <div class="col-md-12">
                                  <div class="form-group">
                                      <label for="naslov">Naslov</label>
                                      <input id="naslov" type="text" class="form-control" placeholder="Naslov in hišna številka" value="${naslov}">
                                  </div>
                              </div>
                              <div class="w-100"></div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="kraj">Mesto</label>
                                      <input id="kraj" type="text" class="form-control" placeholder="Maribor" value="${kraj}">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="posta">Pošta</label>
                                      <input id="posta" type="text" class="form-control" placeholder="2000" value="${posta}">
                                  </div>
                              </div>
                              <div class="w-100"></div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="telefon">Telefon</label>
                                      <input id="telefon" type="text" class="form-control" placeholder="" value="${tel}">
                                  </div>
                              </div>
                              <div class="col-md-6">
                                  <div class="form-group">
                                      <label for="email">E-poštni naslov</label>
                                      <input id="email" type="text" class="form-control" placeholder="" value="${email}">
                                  </div>
                              </div>
                          </div>
                      </form><!-- END -->

                  </div> <!-- .col-md-8 -->

                  <div class="col-md-4">
                      <div class="cart-detail cart-total ftco-bg-dark p-3 p-md-4">
                          <h3 class="billing-heading mb-4">Plačilo skupaj</h3>
                          <p class="d-flex">
                              <span>Znesek</span>
                              <span>${skupniZnesek} €</span>
                          </p>
                          <p class="d-flex">
                              <span>Dostava</span>
                              <span>0.00 €</span>
                          </p>
                          <p class="d-flex">
                              <span>Popust</span>
                              <span>0.00 €</span>
                          </p>
                          <hr>
                          <p class="d-flex total-price">
                              <span>SKUPAJ</span>
                              <span>${skupniZnesek} €</span>
                          </p>
                      </div>
                      <div class="w-100"></div>
                      <div class="cart-detail ftco-bg-dark p-3 p-md-4">
                          <h3 class="billing-heading mb-4">Način plačila</h3>
                          <div class="form-group">
                              <div class="col-md-12">
                                  <div class="radio">
                                      <label><input type="radio" name="optradio" class="mr-2" checked> Ob prevzetju</label>
                                  </div>
                              </div>
                          </div>

                          <div class="form-group">
                              <div class="col-md-12">
                                  <div class="checkbox">
                                      <label><input type="checkbox" value="" class="mr-2"> Sem prebral/a in se strinjam s pogoji poslovanja</label>
                                  </div>
                              </div>
                          </div>
                          <p><a href="posljiNarocilo"class="btn btn-primary py-3 px-4">WUUU tacoo</a></p>
                      </div>
                  </div>
              </div>
          </div>
      </div>

    <footer class="ftco-footer ftco-section img">
        <div class="overlay"></div>
        <div class="container">
            <div class="row mb-5">
                <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">O nas</h2>
                        <p>Taco je več kot restavracija. Je prostor, kjer lahko v miru spijete prvo jutranjo kavo ob vonju našega sveže pečenega kruha in prav tako v naši kuhinji pripravljenih piškotov.
                            Je prostor, kamor povabite svoje poslovne partnerje in je prostor, kjer si skriti pred očmi radovednežev zaljubljeno zrete v oč</p>
                        <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
                            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
                    <div class="ftco-footer-widget mb-4 ml-md-4">
                        <h2 class="ftco-heading-2">Postrežba</h2>
                        <ul class="list-unstyled">
                            <li><a href="#" class="py-2 d-block">Naročila</a></li>
                            <li><a href="#" class="py-2 d-block">Dostava hrane</a></li>
                            <li><a href="#" class="py-2 d-block">Kvaliteta</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 mb-5 mb-md-5">
                    <div class="ftco-footer-widget mb-4">
                        <h2 class="ftco-heading-2">Imaš vprašanje?</h2>
                        <div class="block-23 mb-3">
                            <ul>
                                <li><span class="icon icon-map-marker"></span><span class="text">Koroška cesta 46, 2000 Maribor</span></li>
                                <li><a href="#"><span class="icon icon-phone"></span><span class="text">+2 392 3929 210</span></a></li>
                                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">pridina.taco@gmail.com</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">

                    <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
                </div>
            </div>
        </div>
    </footer>

  <!-- loader -->
  <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


  <script src="js/jquery.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/jquery.waypoints.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.animateNumber.min.js"></script>
  <script src="js/bootstrap-datepicker.js"></script>
  <script src="js/jquery.timepicker.min.js"></script>
  <script src="js/scrollax.min.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
  <script src="js/google-map.js"></script>
  <script src="js/main.js"></script>

  <script>
		$(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
		});
	</script>


  </body>
</html>