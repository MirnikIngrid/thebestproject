package com.praktikum.taco;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TacoApplicationTests {
    //variables
    public final WebDriver chromeDriver;
    private String path = "http:localhost:8080/";

    //inicializacija chrome driverja s chrome objektom
    public TacoApplicationTests(){
        System.setProperty("webdriver.chrome.driver", "E:\\Chromedriver\\chromedriver.exe");
        chromeDriver = new ChromeDriver();
    }


    //test, ki preverja login formo
    @Test
    public void loginForm(){
        chromeDriver.get(path+"prijava");
        WebElement formElement = chromeDriver.findElement(By.id("prijava_forma"));
        WebElement usernameElement = chromeDriver.findElement(By.id("uporabniskoime"));
        WebElement passwordElement = chromeDriver.findElement(By.id("geslo"));
        usernameElement.sendKeys("nejc");
        passwordElement.sendKeys("nejc123");
        formElement.submit();
    }


    //test, ki preverja error message pri ne veljavnem submitu
    @Test
    public void unCorrectErrorMessageLoginForm(){
        chromeDriver.get(path+"prijava");
        WebElement formElement = chromeDriver.findElement(By.id("prijava_forma"));
        WebElement usernameElement = chromeDriver.findElement(By.id("uporabniskoime"));
        WebElement passwordElement = chromeDriver.findElement(By.id("geslo"));
        usernameElement.sendKeys("123");
        passwordElement.sendKeys("123");
        formElement.submit();
        String errorMessage=chromeDriver.findElement(By.id("napaka")).getText();
        System.out.println(errorMessage);
        String preverimo = "Napaka!!";
        Assert.assertNotEquals(errorMessage, preverimo);
    }

    //test, ki preverja error message pri ne veljavnem submitu
    //uporaba niti zaradi responsa strežnika
    @Test
    public void correctErrorMessageLoginForm() {
        try{chromeDriver.get(path+"prijava");
        WebElement formElement = chromeDriver.findElement(By.id("prijava_forma"));
        WebElement usernameElement = chromeDriver.findElement(By.id("uporabniskoime"));
        WebElement passwordElement = chromeDriver.findElement(By.id("geslo"));
        usernameElement.sendKeys("123");
        passwordElement.sendKeys("123");
        formElement.submit();
        Thread.sleep(2000);
        String errorMessage=chromeDriver.findElement(By.id("napaka")).getText();
        System.out.println(errorMessage);
        String preverimo = "Uporabnik ne obstaja!";
        Assert.assertEquals(errorMessage, preverimo);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
